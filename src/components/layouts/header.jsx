import React from 'react';
import { AppBar, Toolbar, Typography, withStyles } from '@material-ui/core';
import CreateExerciseDialog from '../exercises/dialog';

const styles = {
    flex: { flex: 1 }
}

const Header = ({ classes, muscles, onExerciseCreate }) => {
    return (
        <AppBar position="static">
            <Toolbar>
                <Typography variant="headline" color="inherit" className={classes.flex}>Exercise Database</Typography>

                <CreateExerciseDialog muscles={muscles} onCreate={onExerciseCreate} />
            </Toolbar>
        </AppBar>
    );
};

export default withStyles(styles)(Header);