import React, { Component, Fragment } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { Header, Footer } from './layouts';
import Exercises from './exercises/exercises';
import { muscles, exercises } from '../store';

class App extends Component {
    state = {
        exercises,
        exercise: {},
        editMode: false
        // category: 'back'
    };

    getExercisesByMuscles() {
        const initialExercises = muscles.reduce((exercises, category) => ({
            ...exercises,
            [category]: []
        }), {});


        return Object.entries(
            this.state.exercises.reduce((exercises, exercise) => {
                const { muscles } = exercise;
                exercises[muscles] = [...exercises[muscles], exercise];

                return exercises;
            }, initialExercises)
        );
    }

    handleCategorySelect = (category) => {
        this.setState({ category });
    }
    handleExerciseSelect = (id) => {
        this.setState(({ exercises }) => ({
            exercise: exercises.find(ex => ex.id === id),
            editMode: false
        }));
    }
    handleExerciseCreate = (exercise) => {
        this.setState(({ exercises }) => ({
            exercises: [...exercises, exercise]
        }));
    }
    handleExerciseDelete = (id) => {
        this.setState(({ exercises, exercise, editMode }) => ({
            exercises: exercises.filter(ex => ex.id !== id),
            editMode: exercise.id === id ? false : editMode,
            exercise: exercise.id === id ? {} : exercise
        }));
    }
    handleExerciseSelectEdit = (id) => {
        this.setState(({ exercises }) => ({
            exercise: exercises.find(ex => ex.id === id),
            editMode: true,
        }));
    }
    handleExerciseEdit = (exercise) => {
        this.setState(({ exercises }) => ({
            exercises: [
                ...exercises.filter(ex => ex.id !== exercise.id),
                exercise
            ],
            exercise
        }));
    }

    render() {
        const exercises = this.getExercisesByMuscles();
        const { category, exercise, editMode } = this.state;

        return (
            <Fragment>
                <CssBaseline />

                <Header
                    muscles={muscles}
                    onExerciseCreate={this.handleExerciseCreate}
                />

                <Exercises
                    exercise={exercise}
                    exercises={exercises}
                    category={category}
                    editMode={editMode}
                    muscles={muscles}
                    onSelect={this.handleExerciseSelect}
                    onDelete={this.handleExerciseDelete}
                    onSelectEdit={this.handleExerciseSelectEdit}
                    onEdit={this.handleExerciseEdit}
                />

                <Footer
                    category={category}
                    muscles={muscles}
                    onSelect={this.handleCategorySelect}
                />
            </Fragment>
        );
    }
}

export default App;
