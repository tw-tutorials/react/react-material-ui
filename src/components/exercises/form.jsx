import React, { Component } from 'react';
import { Button, FormControl, InputLabel, MenuItem, Select, TextField } from '@material-ui/core';

class Form extends Component {
    state = this.getInitState();

    getInitState() {
        const { exercise } = this.props;

        return exercise
            ? exercise
            : ({
                title: '',
                description: '',
                muscles: ''
            });
    }

    handleChange = (name) => (event) => {
        this.setState({
            [name]: event.target.value
        });
    }

    handleSubmit = () => {
        // TODO: validate

        this.props.onSubmit({
            id: this.state.title.toLowerCase().replace(/ /g, '-'),
            ...this.state
        });
    }

    render() {
        const { title, description, muscles } = this.state;
        const { classes, exercise, muscles: categories } = this.props;
        // exercise: { title, description, muscles }

        return (
            <form>
                <TextField
                    label="Title"
                    value={title}
                    onChange={this.handleChange('title')}
                    margin="normal"
                    fullWidth
                />
                <br />
                <FormControl fullWidth>
                    <InputLabel htmlFor="muscles">Muscles</InputLabel>
                    <Select value={muscles} onChange={this.handleChange('muscles')}>
                        {categories && categories.map((category) => (
                            <MenuItem
                                key={category}
                                value={category}
                                style={{ textTransform: 'capitalize' }}
                            >
                                {category}
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
                <br />
                <TextField
                    label="Description"
                    value={description}
                    onChange={this.handleChange('description')}
                    margin="normal"
                    multiline
                    rows="4"
                    fullWidth
                />
                <br />
                <Button
                    color="primary"
                    variant="contained"
                    onClick={this.handleSubmit}
                    disabled={!title || !muscles}
                >
                    {exercise ? 'Edit' : 'Create'}
                </Button>
            </form>
        );
    }
}

export default Form;