import React, { Fragment } from 'react';
import { Grid, IconButton, List, ListItem, ListItemSecondaryAction, ListItemText, Paper, Typography, withStyles } from '@material-ui/core';
import { Delete, Edit } from '@material-ui/icons';
import Form from './form';

const styles = (theme) => ({
    paper: {
        padding: theme.spacing.unit * 3,
        overflowY: 'auto',
        [theme.breakpoints.up('sm')]: {
            marginTop: 10,
            height: 'calc(100% - 2*10px)'
        },
        [theme.breakpoints.down('xs')]: {
            height: '100%'
        }
    },
    '@global': {
        'html, body, #root': {
            height: '100%'
        }
    },
    'container': {
        [theme.breakpoints.up('sm')]: {
            height: 'calc(100% - 64px - 48px)'
        },
        [theme.breakpoints.down('xs')]: {
            height: 'calc(100% - 56px - 48px)'
        }
    },
    item: {
        [theme.breakpoints.down('xs')]: {
            height: '50%'
        }
    }
})

const Exercises = ({
    classes,
    exercises,
    category,
    editMode,
    muscles,
    onSelect,
    exercise,
    exercise: {
        id,
        title = 'Welcome',
        description = 'Please select an exercise from the list on the left.'
    },
    onDelete,
    onSelectEdit,
    onEdit
}
) => {
    return (
        <Grid container spacing={16} className={classes.container}>
            <Grid item xs={12} sm={6} className={classes.item}>
                <Paper className={classes.paper}>
                    {exercises && exercises.map(([group, exercises]) => (
                        !category || category === group
                            ? (
                                <Fragment key={group}>
                                    <Typography
                                        variant="headline"
                                        style={{ textTransform: 'capitalize' }}
                                        color="secondary"
                                    >
                                        {group}
                                    </Typography>
                                    <List component="ul">
                                        {exercises && exercises.map(({ id, title }) => (
                                            <ListItem
                                                key={id}
                                                button
                                                onClick={() => { onSelect(id); }}
                                            >
                                                <ListItemText primary={title} />
                                                <ListItemSecondaryAction>
                                                    <IconButton onClick={() => onSelectEdit(id)} color="primary">
                                                        <Edit />
                                                    </IconButton>
                                                    <IconButton onClick={() => onDelete(id)} color="primary">
                                                        <Delete />
                                                    </IconButton>
                                                </ListItemSecondaryAction>
                                            </ListItem>
                                        ))}
                                    </List>
                                </Fragment>
                            )
                            : null
                    ))}
                </Paper>
            </Grid>
            <Grid item xs={12} sm={6} className={classes.item}>
                <Paper className={classes.paper}>
                    <Typography
                        variant="display1"
                        gutterBottom
                        color="secondary"
                    >
                        {title}
                    </Typography>
                    {editMode
                        ? (
                            <Form
                                key={id}
                                exercise={exercise}
                                muscles={muscles}
                                onSubmit={onEdit}
                            />
                        )
                        : (
                            <Typography
                                variant="subheading"
                                style={{ marginTop: 20 }}
                            >
                                {description}
                            </Typography>
                        )
                    }
                </Paper>
            </Grid>
        </Grid>
    );
};

export default withStyles(styles)(Exercises);